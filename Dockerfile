FROM python:3.10.4-slim-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
WORKDIR /app

RUN apt update && apt install -y --no-install-recommends gcc g++ python3-dev openssh-client git postgresql-client libpq-dev jq && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/*
RUN pip --no-cache-dir install --upgrade pip

ADD requirements.txt ./
RUN pip install -r requirements.txt

ADD . ./
RUN chmod 775 start.sh
# ENTRYPOINT [ "/app/start.sh" ]
# CMD [] 

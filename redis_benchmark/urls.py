"""redis_benchmark URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from socket import timeout
from django.contrib import admin
from django.urls import path
from django.http.response import HttpResponse
from redis import Redis
from rediscluster import RedisCluster
from pottery.redlock import Redlock
from redis.lock import Lock

redis_connection = None
redis_cluster_connection = None

N = 0

def get_redis_connection():
    global redis_connection
    if not redis_connection:
        redis_connection = Redis(host='redis', port=6379)
    return redis_connection

def get_redis_cluster_connection():
    global redis_connection
    if not redis_connection:
        startup_nodes = [{"host": "redis-cluster", "port": port} for port in range(7000, 7006)]
        redis_connection = RedisCluster(startup_nodes=startup_nodes)
    return redis_connection

def set_count_redis():
    conn = get_redis_connection()
    n = int(conn.get("n") or 0)
    conn.set("n", n + 1)

def test_view(request):
    # conn = get_redis_connection()
    lock_key = "lock_key"
    set_count_redis()
    # if not conn.get(lock_key):
    # global N
    # conn.set(lock_key, "test", ex=2)
    # N += 1
        # conn.delete(lock_key)
    return HttpResponse("OK", status=200)
    # return HttpResponse("Failed", status=400)

def test_single_lock(request):
    """With pottery redlock"""
    conn = get_redis_connection()
    lock_key = "lock_key"
    lock = Lock(conn, lock_key, timeout=10)
    with lock:
        set_count_redis()
        # global N
        # N += 1
        # lock.release()
        return HttpResponse("OK", status=200)
    # return HttpResponse("Failed", status=400)

def test_single_redlock(request):
    """With pottery redlock"""
    conn = get_redis_connection()
    lock_key = "lock_key"
    redlock = Redlock(key=lock_key, masters={conn}, auto_release_time=10 * 1000)
    with redlock:
        set_count_redis()
        # global N
        # N += 1
        # redlock.release()
        return HttpResponse("OK", status=200)
    # return HttpResponse("Failed", status=400)

def test_redis_cluster_redlock(request):
    conn = get_redis_cluster_connection()
    lock_key = "lock_key"
    redlock = Redlock(key=lock_key, masters={conn}, auto_release_time= 10 * 1000)
    with redlock:
        set_count_redis()
        # global N
        # N += 1
        # redlock.release()
        return HttpResponse("OK", status=200)
    # return HttpResponse("Failed", status=400)

def reset(req):
    global N
    N = 0
    return HttpResponse("OK", status=200)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', test_view),
    path('test-single-lock/', test_single_lock),
    path('test-single-redlock/', test_single_redlock),
    path('test-redis-cluster-redlock/', test_redis_cluster_redlock),
    path('reset/', reset),
    path('get/', lambda req: HttpResponse(N, status=200)),
]
